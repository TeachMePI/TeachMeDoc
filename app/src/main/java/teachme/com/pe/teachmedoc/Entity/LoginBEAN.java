package teachme.com.pe.teachmedoc.Entity;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginBEAN {
	private String user;
	private String password;
	private String tipo = "DOC";

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getTipo() {
		return tipo;
	}
}
