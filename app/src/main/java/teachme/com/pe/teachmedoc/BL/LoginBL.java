package teachme.com.pe.teachmedoc.BL;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import teachme.com.pe.teachmedoc.Entity.LoginBEAN;

public class LoginBL {
    public String validaLogin(LoginBEAN loginBEAN){
        final Gson gson = new Gson();
        final String loginJson = gson.toJson(loginBEAN);
        System.out.println("Login : " + loginJson);
        StringBuilder totallinea = new StringBuilder();
        try{
            //Primero creamos la url donde seacremos los datos
            URL service = new URL("http://192.168.1.3:8081/TeachMe/validaLogin");
            //Comenzaremos creando la conexion
            HttpURLConnection cn = (HttpURLConnection)service.openConnection();
            //tiempo de espera
            cn.setConnectTimeout(20*1000);
            //como se enviara el request
            cn.setRequestProperty("Content-Type","application/json");

            cn.setDoOutput(true);
            cn.setDoInput(true);
            cn.setRequestMethod("POST");

            OutputStream os = cn.getOutputStream();
            os.write(loginJson.getBytes("UTF-8"));
            os.close();

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(cn.getInputStream(),"UTF-8"));
            String linea;

            while((linea = in.readLine())!=null){
                totallinea .append(linea);
            }
            in.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return totallinea.toString();
    }
}
