package teachme.com.pe.teachmedoc.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import teachme.com.pe.teachmedoc.Entity.EventoEntity;
import teachme.com.pe.teachmedoc.R;

public class EventoAdapter extends RecyclerView.Adapter<EventoAdapter.ViewHolder> implements View.OnClickListener {
    public List<EventoEntity> eventoEntities;
    private View.OnClickListener listener;

    public EventoAdapter(List<EventoEntity> anuncioEntities) {
        this.eventoEntities = anuncioEntities;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_event_calander, parent,false);
        view.setOnClickListener(this);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull EventoAdapter.ViewHolder holder, int position) {
        holder.txtHoraEvento.setText(eventoEntities.get(position).getHora());
        holder.txtMateria.setText(eventoEntities.get(position).getMateria());
        holder.txtLugar.setText(eventoEntities.get(position).getLugar());
    }

    @Override
    public int getItemCount() {
        return eventoEntities.size();
    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        if(listener != null){
            listener.onClick(v);
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        CardView crdEventoHorario;
        TextView txtHoraEvento;
        TextView txtMateria;
        TextView txtLugar;

        public ViewHolder(View itemView) {
            super(itemView);
            crdEventoHorario = (CardView)itemView.findViewById(R.id.crdEventoHorario);
            txtHoraEvento = (TextView)itemView.findViewById(R.id.txtHoraEvento);
            txtMateria = (TextView)itemView.findViewById(R.id.txtMateria);
            txtLugar = (TextView)itemView.findViewById(R.id.txtLugar);
        }
    }
}
