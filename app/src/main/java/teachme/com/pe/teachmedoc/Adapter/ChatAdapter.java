package teachme.com.pe.teachmedoc.Adapter;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import teachme.com.pe.teachmedoc.Entity.ChatEntity;
import teachme.com.pe.teachmedoc.R;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> implements View.OnClickListener {
public List<ChatEntity> chatEntities;
private View.OnClickListener listener;

public ChatAdapter(List<ChatEntity> chatEntities) {
        this.chatEntities = chatEntities;
        }

@NonNull
@Override
public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_list, parent,false);
        view.setOnClickListener(this);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
        }

@Override
public void onBindViewHolder(@NonNull ChatAdapter.ViewHolder holder, int position) {
        holder.txtMensajeChatItem.setText(chatEntities.get(position).getMensaje());
        holder.txtFechaUltimoMensajeItem.setText(chatEntities.get(position).getFecha());
        holder.txtNombrePersonaChat.setText(chatEntities.get(position).getPersona());
        int color = Color.parseColor("#04B404");
        holder.imgConectItemChat.setColorFilter(color);
        if(chatEntities.get(position).isConectado()){
            holder.imgConectItemChat.setVisibility(View.INVISIBLE);
        }else {
            holder.imgConectItemChat.setVisibility(View.VISIBLE);
        }
        }

@Override
public int getItemCount() {
        return chatEntities.size();
        }

public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;
        }

@Override
public void onClick(View v) {
        if(listener != null){
        listener.onClick(v);
        }
        }

public static class ViewHolder extends RecyclerView.ViewHolder{
    CardView crdChat;
    TextView txtMensajeChatItem;
    TextView txtFechaUltimoMensajeItem;
    ImageView imgChatItem;
    ImageView imgConectItemChat;
    TextView txtNombrePersonaChat;

    public ViewHolder(View itemView) {
        super(itemView);
        crdChat = (CardView)itemView.findViewById(R.id.crdChat);
        txtMensajeChatItem = (TextView)itemView.findViewById(R.id.txtMensajeChatItem);
        txtFechaUltimoMensajeItem = (TextView)itemView.findViewById(R.id.txtFechaUltimoMensajeItem);
        txtNombrePersonaChat = (TextView)itemView.findViewById(R.id.txtNombrePersonaChat);
        imgChatItem = (ImageView) itemView.findViewById(R.id.imgChatItem);
        imgConectItemChat = (ImageView) itemView.findViewById(R.id.imgConectItemChat);
    }
}
}
