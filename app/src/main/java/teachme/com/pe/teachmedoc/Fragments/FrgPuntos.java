package teachme.com.pe.teachmedoc.Fragments;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.DefaultAxisValueFormatter;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;
import java.util.Map;

import teachme.com.pe.teachmedoc.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FrgPuntos.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FrgPuntos#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FrgPuntos extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FrgPuntos() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FrgPuntos.
     */
    // TODO: Rename and change types and number of parameters
    public static FrgPuntos newInstance(String param1, String param2) {
        FrgPuntos fragment = new FrgPuntos();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    LineChart lineChart;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_frg_puntos, container, false);
        lineChart = (LineChart)view.findViewById(R.id.chartPuntos);
        lineChart.setDragEnabled(true);
        lineChart.setScaleEnabled(true);
        ArrayList<Entry> entries = new ArrayList<>();
        entries.add(new Entry(1,5f));
        entries.add(new Entry(2,8f));
        entries.add(new Entry(3,14f));
        entries.add(new Entry(4,11f));
        entries.add(new Entry(5,2f));
        LineDataSet lineDataSet = new LineDataSet(entries, "Puntaje");
        //lineDataSet.setFillAlpha(110);
        lineDataSet.setColor(Color.BLUE);
        //lineDataSet.setLineWidth(4f);
        lineDataSet.setValueTextColor(Color.DKGRAY);
        ArrayList<ILineDataSet> iLineDataSets = new ArrayList<>();
        iLineDataSets.add(lineDataSet);
        LineData lineData = new LineData(iLineDataSets);
        lineChart.getAxisRight().setEnabled(false);
        lineChart.setData(lineData);
        String[] values = new String[] {"S1","S2","S3","S4","S5"};
        XAxis xAxis = lineChart.getXAxis();
        xAxis.setGranularity(1f);
        //xAxis.setValueFormatter(new MyAxisValueFormatter(values));

        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        return view;
    }

    public  class MyAxisValueFormatter implements IAxisValueFormatter{
        private String[] nvalues;
        public MyAxisValueFormatter(String[] values){
            this.nvalues = values;
        }


        @Override
        public String getFormattedValue(float value, AxisBase axis) {
            //System.out.println(nvalues.length + " - " + value);
            return nvalues[(int)value];
        }
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
