package teachme.com.pe.teachmedoc.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import teachme.com.pe.teachmedoc.Entity.AnuncioEntity;
import teachme.com.pe.teachmedoc.R;

public class AnuncioAdapter extends RecyclerView.Adapter<AnuncioAdapter.ViewHolder> implements View.OnClickListener {
    public List<AnuncioEntity> anuncioEntities;
    private View.OnClickListener listener;

    public AnuncioAdapter(List<AnuncioEntity> anuncioEntities) {
        this.anuncioEntities = anuncioEntities;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_anuncio, parent,false);
        view.setOnClickListener(this);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull AnuncioAdapter.ViewHolder holder, int position) {
        holder.txtAnuncioTitulo.setText(anuncioEntities.get(position).getTitulopAnuncio());
        holder.txtAnuncioLugar.setText(anuncioEntities.get(position).getLugarAnuncio());
        holder.txtAnuncioMateria.setText(anuncioEntities.get(position).getMateriaAnuncio());
        holder.txtAnuncioFecha.setText(anuncioEntities.get(position).getFechaAnuncio());
    }

    @Override
    public int getItemCount() {
        return anuncioEntities.size();
    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        if(listener != null){
            listener.onClick(v);
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        CardView crdContainerA;
        ImageView imgPerson;
        TextView txtAnuncioTitulo;
        TextView txtAnuncioLugar;
        TextView txtAnuncioMateria;
        TextView txtAnuncioFecha;

        public ViewHolder(View itemView) {
            super(itemView);
            crdContainerA = (CardView)itemView.findViewById(R.id.crdContainerA);
            txtAnuncioTitulo = (TextView)itemView.findViewById(R.id.txtAnuncioTitulo);
            txtAnuncioLugar = (TextView)itemView.findViewById(R.id.txtAnuncioLugar);
            txtAnuncioMateria = (TextView)itemView.findViewById(R.id.txtAnuncioMateria);
            txtAnuncioFecha = (TextView)itemView.findViewById(R.id.txtAnuncioFecha);
        }
    }
}
