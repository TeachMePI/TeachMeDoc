package teachme.com.pe.teachmedoc.Entity;

public class DocenteBEAN {
	private int docenteID;
	private String docenteNom;
	private String docentePat;
	private String docenteMat;
	private int docenteEdad;
	private String docenteEmail;
	private String docenteFoto;
	private String docenteUsu;
	private String docentePass;
	private String docenteDesc;
	private int docentePunt;
	private int docenteSex;

	public DocenteBEAN(int docenteID, String docenteNom, String docentePat, String docenteMat, int docenteEdad,
			String docenteEmail, String docenteFoto, String docenteUsu, String docentePass, String docenteDesc,
			int docentePunt, int docenteSex) {
		super();
		this.docenteID = docenteID;
		this.docenteNom = docenteNom;
		this.docentePat = docentePat;
		this.docenteMat = docenteMat;
		this.docenteEdad = docenteEdad;
		this.docenteEmail = docenteEmail;
		this.docenteFoto = docenteFoto;
		this.docenteUsu = docenteUsu;
		this.docentePass = docentePass;
		this.docenteDesc = docenteDesc;
		this.docentePunt = docentePunt;
		this.docenteSex = docenteSex;
	}

	public int getDocenteID() {
		return docenteID;
	}

	public void setDocenteID(int docenteID) {
		this.docenteID = docenteID;
	}

	public String getDocenteNom() {
		return docenteNom;
	}

	public void setDocenteNom(String docenteNom) {
		this.docenteNom = docenteNom;
	}

	public String getDocentePat() {
		return docentePat;
	}

	public void setDocentePat(String docentePat) {
		this.docentePat = docentePat;
	}

	public String getDocenteMat() {
		return docenteMat;
	}

	public void setDocenteMat(String docenteMat) {
		this.docenteMat = docenteMat;
	}

	public int getDocenteEdad() {
		return docenteEdad;
	}

	public void setDocenteEdad(int docenteEdad) {
		this.docenteEdad = docenteEdad;
	}

	public String getDocenteEmail() {
		return docenteEmail;
	}

	public void setDocenteEmail(String docenteEmail) {
		this.docenteEmail = docenteEmail;
	}

	public String getDocenteFoto() {
		return docenteFoto;
	}

	public void setDocenteFoto(String docenteFoto) {
		this.docenteFoto = docenteFoto;
	}

	public String getDocenteUsu() {
		return docenteUsu;
	}

	public void setDocenteUsu(String docenteUsu) {
		this.docenteUsu = docenteUsu;
	}

	public String getDocentePass() {
		return docentePass;
	}

	public void setDocentePass(String docentePass) {
		this.docentePass = docentePass;
	}

	public String getDocenteDesc() {
		return docenteDesc;
	}

	public void setDocenteDesc(String docenteDesc) {
		this.docenteDesc = docenteDesc;
	}

	public int getDocentePunt() {
		return docentePunt;
	}

	public void setDocentePunt(int docentePunt) {
		this.docentePunt = docentePunt;
	}

	public int getDocenteSex() {
		return docenteSex;
	}

	public void setDocenteSex(int docenteSex) {
		this.docenteSex = docenteSex;
	}

	

}
