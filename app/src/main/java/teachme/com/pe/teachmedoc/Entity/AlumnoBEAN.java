package teachme.com.pe.teachmedoc.Entity;

public class AlumnoBEAN {
	private int alumnoID;
	private String alumnoNom;
	private String alumnoPat;
	private String alumnoMat;
	private int alumnoEdad;
	private String alumnoTelf;
	private String alumnoEmail;
	private String alumnoFoto;
	private String alumnoUsu;
	private String alumnoPass;
	private int alumnoSexo;

	public AlumnoBEAN(int alumnoID, String alumnoNom, String alumnoPat, String alumnoMat, int alumnoEdad,
			String alumnoTelf, String alumnoEmail, String alumnoFoto, String alumnoUsu, String alumnoPass,
			int alumnoSexo) {
		super();
		this.alumnoID = alumnoID;
		this.alumnoNom = alumnoNom;
		this.alumnoPat = alumnoPat;
		this.alumnoMat = alumnoMat;
		this.alumnoEdad = alumnoEdad;
		this.alumnoTelf = alumnoTelf;
		this.alumnoEmail = alumnoEmail;
		this.alumnoFoto = alumnoFoto;
		this.alumnoUsu = alumnoUsu;
		this.alumnoPass = alumnoPass;
		this.alumnoSexo = alumnoSexo;
	}

	public int getAlumnoID() {
		return alumnoID;
	}

	public void setAlumnoID(int alumnoID) {
		this.alumnoID = alumnoID;
	}

	public String getAlumnoNom() {
		return alumnoNom;
	}

	public void setAlumnoNom(String alumnoNom) {
		this.alumnoNom = alumnoNom;
	}

	public String getAlumnoPat() {
		return alumnoPat;
	}

	public void setAlumnoPat(String alumnoPat) {
		this.alumnoPat = alumnoPat;
	}

	public String getAlumnoMat() {
		return alumnoMat;
	}

	public void setAlumnoMat(String alumnoMat) {
		this.alumnoMat = alumnoMat;
	}

	public int getAlumnoEdad() {
		return alumnoEdad;
	}

	public void setAlumnoEdad(int alumnoEdad) {
		this.alumnoEdad = alumnoEdad;
	}

	public String getAlumnoTelf() {
		return alumnoTelf;
	}

	public void setAlumnoTelf(String alumnoTelf) {
		this.alumnoTelf = alumnoTelf;
	}

	public String getAlumnoEmail() {
		return alumnoEmail;
	}

	public void setAlumnoEmail(String alumnoEmail) {
		this.alumnoEmail = alumnoEmail;
	}

	public String getAlumnoFoto() {
		return alumnoFoto;
	}

	public void setAlumnoFoto(String alumnoFoto) {
		this.alumnoFoto = alumnoFoto;
	}

	public String getAlumnoUsu() {
		return alumnoUsu;
	}

	public void setAlumnoUsu(String alumnoUsu) {
		this.alumnoUsu = alumnoUsu;
	}

	public String getAlumnoPass() {
		return alumnoPass;
	}

	public void setAlumnoPass(String alumnoPass) {
		this.alumnoPass = alumnoPass;
	}

	public int getAlumnoSexo() {
		return alumnoSexo;
	}

	public void setAlumnoSexo(int alumnoSexo) {
		this.alumnoSexo = alumnoSexo;
	}

}
