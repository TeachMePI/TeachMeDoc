package teachme.com.pe.teachmedoc;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.gson.JsonIOException;

import org.json.JSONException;
import org.json.JSONObject;

import teachme.com.pe.teachmedoc.BL.LoginBL;
import teachme.com.pe.teachmedoc.Entity.LoginBEAN;
import teachme.com.pe.teachmedoc.Utils.UtilFunctions;

public class LoginActivity extends AppCompatActivity {

    View view;
    Button btnIngresar;
    TextInputLayout txtUser;
    TextInputLayout txtPass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        btnIngresar = (Button)findViewById(R.id.btnIngresar);
        txtUser = findViewById(R.id.txtUser);
        txtPass = findViewById(R.id.txtPass);
        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user = txtUser.getEditText().getText().toString().trim();
                String pass = txtPass.getEditText().getText().toString();
                LoginBEAN loginBEAN = new LoginBEAN();
                loginBEAN.setUser(user);
                loginBEAN.setPassword(pass);
                System.out.println("hola : " + user + " " + pass);
                new Asynclogin().execute(loginBEAN);
            }
        });
    }


    private class Asynclogin extends AsyncTask<LoginBEAN, Integer, String>{
        ProgressDialog dlg = new ProgressDialog(LoginActivity.this);
        @Override
        protected void onPreExecute() {
            dlg.setTitle("TeachMe");
            dlg.setMessage("Validando usuario...");
            dlg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dlg.show();
        }

        @Override
        protected String doInBackground(LoginBEAN... loginBEANS) {
            LoginBL loginBL = new LoginBL();
            String docente = loginBL.validaLogin(loginBEANS[0]);
            return docente;
        }

        @Override
        protected void onPostExecute(String s) {
            if(dlg.isShowing()) dlg.dismiss();
            String data = null;
            String mensaje = null;
            try {
                JSONObject jsonObject = new JSONObject(s);
                data = jsonObject.getString("data");
                mensaje = jsonObject.getString("mensaje");
                System.out.println(jsonObject.toString());
            }catch (JSONException e){
                System.out.println("Error valida login onPostExecute: " + e);
            }

            if(data.equals("null")){
                AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
                alertDialog.setTitle("TeachMe");
                alertDialog.setMessage(mensaje);
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }else{
                saveLoginPreferense(data);
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
            }
        }
    }

    private void saveLoginPreferense(String cod){
        SharedPreferences sharedPreferences = getSharedPreferences("login_preferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("codUsuario", cod);
        editor.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("ONRESUME");
        UtilFunctions utilFunctions = new UtilFunctions(getApplicationContext());
        System.out.println("hh:" + utilFunctions.isOnlineNet());
        if (utilFunctions.isOnlineNet() == false){
           /* Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();*/
            AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
            alertDialog.setTitle("NO TIENE INTERNET");
            alertDialog.setMessage("NO TIENE ACCESO A INTERNET... :(");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
        }
    }
}
