package teachme.com.pe.teachmedoc.Entity;

public class AnuncioEntity {
    String idAnuncio;
    String titulopAnuncio;
    String lugarAnuncio;
    String materiaAnuncio;
    String fechaAnuncio;
    String descripion;

    public AnuncioEntity(String idAnuncio, String titulopAnuncio, String lugarAnuncio, String materiaAnuncio, String fechaAnuncio, String descripion) {
        this.idAnuncio = idAnuncio;
        this.titulopAnuncio = titulopAnuncio;
        this.lugarAnuncio = lugarAnuncio;
        this.materiaAnuncio = materiaAnuncio;
        this.fechaAnuncio = fechaAnuncio;
        this.descripion = descripion;
    }

    public String getIdAnuncio() {
        return idAnuncio;
    }

    public void setIdAnuncio(String idAnuncio) {
        this.idAnuncio = idAnuncio;
    }

    public String getTitulopAnuncio() {
        return titulopAnuncio;
    }

    public void setTitulopAnuncio(String titulopAnuncio) {
        this.titulopAnuncio = titulopAnuncio;
    }

    public String getLugarAnuncio() {
        return lugarAnuncio;
    }

    public void setLugarAnuncio(String lugarAnuncio) {
        this.lugarAnuncio = lugarAnuncio;
    }

    public String getMateriaAnuncio() {
        return materiaAnuncio;
    }

    public void setMateriaAnuncio(String materiaAnuncio) {
        this.materiaAnuncio = materiaAnuncio;
    }

    public String getFechaAnuncio() {
        return fechaAnuncio;
    }

    public void setFechaAnuncio(String fechaAnuncio) {
        this.fechaAnuncio = fechaAnuncio;
    }

    public String getDescripion() {
        return descripion;
    }

    public void setDescripion(String descripion) {
        this.descripion = descripion;
    }
}
