package teachme.com.pe.teachmedoc.Entity;

public class ChatEntity {
    String id;
    String mensaje;
    String fecha;
    String persona;
    boolean conectado;

    public ChatEntity(String id, String mensaje, String fecha, String persona, boolean conectado) {
        this.id = id;
        this.mensaje = mensaje;
        this.fecha = fecha;
        this.persona = persona;
        this.conectado = conectado;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getPersona() {
        return persona;
    }

    public void setPersona(String persona) {
        this.persona = persona;
    }

    public boolean isConectado() {
        return conectado;
    }

    public void setConectado(boolean conectado) {
        this.conectado = conectado;
    }
}
