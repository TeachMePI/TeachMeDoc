package teachme.com.pe.teachmedoc.Entity;

public class EventoEntity {
    String id;
    String dia;
    String hora;
    String materia;
    String lugar;

    public EventoEntity(String id, String dia, String hora, String materia, String lugar) {
        this.id = id;
        this.dia = dia;
        this.hora = hora;
        this.materia = materia;
        this.lugar = lugar;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getMateria() {
        return materia;
    }

    public void setMateria(String materia) {
        this.materia = materia;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }
}
