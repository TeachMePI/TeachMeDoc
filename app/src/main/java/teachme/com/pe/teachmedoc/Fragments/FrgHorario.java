package teachme.com.pe.teachmedoc.Fragments;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.Toast;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.CalendarMode;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import java.util.ArrayList;
import java.util.Calendar;

import teachme.com.pe.teachmedoc.Adapter.EventoAdapter;
import teachme.com.pe.teachmedoc.Entity.EventoEntity;
import teachme.com.pe.teachmedoc.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FrgHorario.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FrgHorario#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FrgHorario extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FrgHorario() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FrgHorario.
     */
    // TODO: Rename and change types and number of parameters
    public static FrgHorario newInstance(String param1, String param2) {
        FrgHorario fragment = new FrgHorario();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    MaterialCalendarView calendarView;
    RecyclerView recyclerView;
    EventoAdapter eventoAdapter;
    ArrayList<EventoEntity> eventoEntities;
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_frg_horario, container, false);
        calendarView = (MaterialCalendarView) view.findViewById(R.id.calendarView);
        calendarView.state().edit()
                .setFirstDayOfWeek(Calendar.MONDAY)
                .setMinimumDate(CalendarDay.from(1990, 1, 1))
                .setMaximumDate(CalendarDay.from(2100, 12, 31))
                .setCalendarDisplayMode(CalendarMode.MONTHS)
                .commit();
        calendarView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                int mes = date.getMonth() + 1;
                int dia = date.getDay();
                int anio = date.getYear();
                Toast.makeText(getActivity(), dia + "/" + mes + "/" + anio,Toast.LENGTH_SHORT).show();
            }
        });
        recyclerView = (RecyclerView)view.findViewById(R.id.listEventosHorario);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager ly = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(ly);
        eventoEntities = new ArrayList<>();
        eventoEntities.add(new EventoEntity(null,null, "12:05", "INGLES","LIMA"));
        eventoEntities.add(new EventoEntity(null,null, "13:05", "MATEMATICA","LIMA"));
        eventoEntities.add(new EventoEntity(null,null, "16:05", "CIENCIAS","LIMA"));
        eventoEntities.add(new EventoEntity(null,null, "18:05", "IMFORMATICA","LIMA"));
        eventoEntities.add(new EventoEntity(null,null, "22:05", "JAVA","LIMA"));
        eventoAdapter = new EventoAdapter(eventoEntities);
        eventoAdapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String titulo = eventoEntities.get(recyclerView.getChildAdapterPosition(v)).getMateria();
                Toast.makeText(getActivity(),titulo, Toast.LENGTH_SHORT).show();
            }
        });
        recyclerView.setAdapter(eventoAdapter);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
